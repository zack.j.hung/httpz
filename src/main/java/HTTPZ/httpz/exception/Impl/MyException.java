package HTTPZ.httpz.exception.Impl;

import HTTPZ.httpz.exception.HttpException;

public class MyException extends Throwable implements HttpException {
	private static final long serialVersionUID = -2105843868314575343L;
	private String message = "";
	private int code = 0;
	
	public MyException(String s, int c) {
		setMessage(s);
		setCode(c);
	}
	
	public void setMessage(String s) {
		this.message = s;
	}
	public String getMessage() {
		return this.message;
	}
	public void setCode(int c) {
		this.code = c;
	}
	public int getCode() {
		return this.code;
	}
	
	
}
