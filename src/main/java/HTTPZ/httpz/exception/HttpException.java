package HTTPZ.httpz.exception;


public interface HttpException {
	public void setMessage(String s);
	public String getMessage();
	public void setCode(int c);
	public int getCode();
}
