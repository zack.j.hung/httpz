package HTTPZ.httpz.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.net.URI;

import javax.xml.bind.JAXBException;

import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.Headers;

import HTTPZ.httpz.model.GenericData;
import HTTPZ.httpz.model.getRequest;

@SuppressWarnings("restriction")
public class MyHttpHandler implements HttpHandler {

	private getRequest reqHTML = null;
	private String strHTML = "";
	private String strJSON = "";
	private String strXML = "";
	String responseContent = "Hello World!";
	private Headers responseHeaders = null;

	private void getRequest(HttpExchange exchange) throws IOException, JAXBException {
		URI uri = exchange.getRequestURI();
		String[] pathList;
		String path = uri.getPath();
		StringBuffer sb = new StringBuffer(path);

		sb.deleteCharAt(0);
		path = sb.toString();
		System.out.println(path);
		pathList = path.split("/", 2);
		for (String s : pathList) {
			System.out.println(s);
		}

		reqHTML = new getRequest();
		reqHTML.setExchange(exchange);
		reqHTML.parserRequestInfo();
		if (pathList.length > 1) {
			if (pathList[1].equalsIgnoreCase("json")) {
				strJSON = reqHTML.getJSON();
				responseContent = strJSON;
				responseHeaders.add("Accept", "application/json");
				responseHeaders.add("Content-type", "application/json;charset=utf-8");
				return;
				
			} else if (pathList[1].equalsIgnoreCase("xml")) {
				strXML = reqHTML.getXML(GenericData.class);
				responseContent = strXML;
				responseHeaders.add("Accept", "application/xml");
				responseHeaders.add("Content-type", "application/xml;charset=utf-8");
				return;
			}
		}
		
		strHTML = reqHTML.getHTML();
		responseContent = strHTML;
		responseHeaders.add("Accept", "text/html");
		responseHeaders.add("Content-type", "text/html;charset=utf-8");

	}

	private void errorResponse(HttpExchange exchange) throws IOException {
		String base = "http://" + exchange.getRequestHeaders().getFirst("Host") + "/test";
		StringBuffer sb = new StringBuffer();
		sb.append("<html>Please try this way<br>");
		sb.append(base + "/html<br>");
		sb.append(base + "/json<br>");
		sb.append(base + "/xml</html>");

		String response = sb.toString();
		exchange.sendResponseHeaders(405, 0);
		exchange.getResponseHeaders().add("Accept", "text/html");
		exchange.getResponseHeaders().add("Content-type", "text/html;charset=utf-8");
		OutputStream os = exchange.getResponseBody();
		os.write(response.getBytes());
		os.close();
	}

	private void sendResponse(HttpExchange exchange) throws IOException {
		exchange.sendResponseHeaders(200, 0);
		OutputStream os = exchange.getResponseBody();
		os.write(responseContent.getBytes());
		os.close();
	}

	public void handle(HttpExchange exchange) {
		responseHeaders = exchange.getResponseHeaders();
		try {
			getRequest(exchange);
			sendResponse(exchange);
		} catch (JAXBException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
