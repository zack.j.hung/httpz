package HTTPZ.httpz;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.URI;

import com.sun.net.httpserver.HttpServer;

import HTTPZ.httpz.controller.MyHttpHandler;
import HTTPZ.httpz.exception.Impl.MyException;

@SuppressWarnings("restriction")
public class HttpContainer extends Thread {

	private int port = 40001;
	private String host = "";
	private URI uri = null;
	HttpServer server = null;

	HttpContainer(String host, int port) {
		this.port = port;
		this.host = host;
	}

	private void initConention(String path) throws MyException {
		try {
			server = HttpServer.create(new InetSocketAddress(this.port), 0);
			server.createContext(path, new MyHttpHandler());
			server.setExecutor(null);
			server.start();
		} catch (IOException e) {
			// throw new MyException("Test fail.", 3333);
			e.printStackTrace();
		}

	}

	public void run() {
		try {
			initConention("/test");
		} catch (MyException e) {
			e.printStackTrace();
		}
	}

}

