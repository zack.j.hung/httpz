package HTTPZ.httpz;


public class App {
	public static void main(String[] args) throws InterruptedException {
		HttpContainer httpContainer = new HttpContainer("localhost", 40001);
		httpContainer.start();
		Thread.currentThread().join();
	}
}
