package HTTPZ.httpz.model;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.URI;

import javax.xml.bind.JAXBException;

import com.sun.net.httpserver.HttpExchange;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.sun.net.httpserver.Headers;
import lombok.Getter;
import lombok.Setter;

@SuppressWarnings("restriction")
@Getter
@Setter
public abstract class RootRequest {
	private HttpExchange exchange = null;
	private GenericData  genericData = null;
	
	abstract public String getHTML();

	abstract public String getJSON();

	abstract public String getXML(Class c) throws JsonProcessingException, JAXBException;

	public void parserRequestInfo() throws IOException {
		String requestURL = "";
		Integer contentSize = 0;
		InetSocketAddress remoteIP = exchange.getRemoteAddress();
		Headers exHeaders = exchange.getRequestHeaders();
		
		genericData.setRemoteAddress(remoteIP.getHostName());
		genericData.setProtocol(exchange.getProtocol());
		genericData.setMethod(exchange.getRequestMethod());
		System.out.println("Remote Host Name:" + remoteIP.getHostName());
		System.out.println("Request Method:" + exchange.getRequestMethod());
		System.out.println("Protocol:" + exchange.getProtocol());
		
		
		URI uri = exchange.getRequestURI();
		requestURL = "http://" + exHeaders.getFirst("Host");
		
		if (uri.getPath() != null) {
			if (!uri.getPath().isEmpty()) {
				requestURL += uri.getPath();
			}
		}

		if (uri.getRawQuery() != null) {
			if (!uri.getRawQuery().isEmpty()) {
				System.out.println("getRawQuery=" + uri.getRawQuery());
				System.out.println("getRawQuery=" + uri.getQuery());
				requestURL += "?" + uri.getRawQuery();
				genericData.setQueryParameter(uri.getRawQuery().split("&"));
			}
		}
		genericData.setRequestURL(requestURL);
		System.out.println("requestURL=" + requestURL);
		

		
		String contentValue = exHeaders.getFirst("Content-length");
		System.out.println("contentValue=" + contentValue);
		if (contentValue != null) {
			contentSize = Integer.valueOf(contentValue);
		} else {
			contentSize = 0;
		}

		/*List<String> cookies = exHeaders.get("Cookie");
		if (cookies != null) {
			System.out.println("Cookie:");
			for (String s : cookies) {
				System.out.println(s);
			}
		}*/

		// print all header
		if (!exHeaders.isEmpty()) {
			System.out.println("Header:");
			for (String key : exHeaders.keySet()) {
				genericData.getRequestHeaders().put(key, exHeaders.getFirst(key));
				System.out.println(key + ":" + exHeaders.getFirst(key));
			}

		} else {
			System.out.println("Header: no header data.");
		}

		// print content data
		if (contentSize > 0) {
			InputStream is = exchange.getRequestBody();
			genericData.setRequestBODY(readContent(is, contentSize).toString());
			System.out.println(genericData.getRequestBODY().toString());
		}
	}

	private StringBuffer readContent(InputStream is, Integer size) throws IOException {
		StringBuffer sb = new StringBuffer();
		byte[] b = new byte[1024 * 1024];
		int pointer = b.length;

		while (is.read(b, 0, b.length) > 0) {
			if (pointer < size)
				size -= pointer;
			else
				pointer = size;

			for (int i = 0; i < pointer; i++) {
				sb.append((char) b[i]);
			}
		}

		return sb;
	}

}
