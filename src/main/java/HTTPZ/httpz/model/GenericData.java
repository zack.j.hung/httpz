package HTTPZ.httpz.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlCData;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
//@JsonIgnoreProperties({ "exchange" })
@JacksonXmlRootElement(localName = "XMLRequestFormat")
//@XmlRootElement(name = "XMLRequestFormat")
//@XmlAccessorType(XmlAccessType.FIELD)
@JsonRootName(value = "JSONRequestFormat")
@JsonInclude(Include.NON_NULL)
@JsonPropertyOrder(
	{ "method", "protocol",  "requestURL", "remoteAddress", "queryParameter", "requestHeaders", "requestBODY"})
public class GenericData {
	
	//@XmlElement(name = "XmlMethod")
	//@JacksonXmlProperty(localName = "XmlMethod", isAttribute = true)
	@JsonProperty("Method")
	private String method = "";
	
	//@JacksonXmlProperty(localName = "XmlProtocol", isAttribute = true)
	//@XmlElement(name = "XmlProtocol")
	@JsonProperty("Protocol")
	private String protocol = "";
	
	//@JacksonXmlProperty(localName = "QueryParameters", isAttribute = true)
	//@XmlElement(name = "XmlQueryParameters")
	//@JacksonXmlElementWrapper(localName="XmlQueryParameters")
	@JsonProperty("QueryParameters")
	private String[] queryParameter = null;
	
	//@JacksonXmlProperty(localName = "XmlURL", isAttribute = true)
	//@XmlElement(name = "XmlURL")
	@JsonProperty("URL")
	private String requestURL = "";
	
	//@JacksonXmlProperty(localName = "XmlRemoteAddress", isAttribute = true)
	//@XmlElement(name = "XmlRemoteAddress")
	@JsonProperty("RemoteAddress")
	private String remoteAddress = "";
	
	//@JacksonXmlProperty(localName = "XmlBody", isAttribute = true)
	//@XmlElement(name = "XmlBody")
	//@JacksonXmlText
	@JacksonXmlCData(value = true)
	@JsonProperty("Body")
	private String requestBODY = null;
	//private StringBuffer requestBODY = null;
	
	//@JacksonXmlElementWrapper(localName = "XmlHeaders")
	//@JacksonXmlProperty(localName = "XmlHeader")
	//@XmlElement(name = "XmlHeaders")
	//@XmlElementWrapper(name="Headers")
    //@XmlElement(name="Header")
	@JsonProperty("Headers")
	private Map<String, String> requestHeaders = new LinkedHashMap<String, String>();
	
}