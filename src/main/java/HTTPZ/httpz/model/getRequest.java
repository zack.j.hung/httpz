package HTTPZ.httpz.model;

import javax.xml.bind.JAXBException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.JacksonXmlModule;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class getRequest extends RootRequest {

	public getRequest() {
		setGenericData(new GenericData());
	}
	
	@Override
	public String getHTML() {
		GenericData genericaData = getGenericData();
		StringBuffer HTMLStyle = new StringBuffer("<!DOCTYPE html><html>");
		
		// General table
		HTMLStyle.append("<p>General</p>");
		HTMLStyle.append("<table style=\"height: 55px; width:80%;border:2px #FFD382 dashed;\" border=\"1\"><tbody>");
		HTMLStyle.append("<tr><td style=\"width: 15%;\">Request URL</td>");
		HTMLStyle.append("<td style=\"width: 65%;\">" + genericaData.getRequestURL() + "</td></tr>");
		HTMLStyle.append("<tr><td>Request Method</td>");
		HTMLStyle.append("<td>" + genericaData.getMethod() + "</td></tr>");
		HTMLStyle.append("<tr><td>Request Protocol</td>");
		HTMLStyle.append("<td>" + genericaData.getProtocol() + "</td></tr>");
		HTMLStyle.append("<tr><td>Remote Address</td>");
		HTMLStyle.append("<td>" + genericaData.getRemoteAddress() + "</td></tr>");
		HTMLStyle.append("</tbody></table>");
		
		// Request Headers table
		//HTMLStyle.append("<p>&nbsp;</p>");
		HTMLStyle.append("<p>Request Headers</p>");
		HTMLStyle.append("<table style=\"height: 55px; width:80%;border: 2px #7BBD6E dashed;\" border=\"1\"><tbody>");
		for (String key : genericaData.getRequestHeaders().keySet()) {
			HTMLStyle.append("<tr><td style=\"width: 30%;\">" + key + "</td>");
			HTMLStyle.append("<td style=\"width: 50%;\">" +
					genericaData.getRequestHeaders().get(key) + "</td></tr>");
		}
		HTMLStyle.append("</tbody></table>");
		
		// Query String Parameters table
		//HTMLStyle.append("<p>&nbsp;</p>");
		HTMLStyle.append("<p>Query String Parameters</p>");
		HTMLStyle.append("<table style=\"height: 55px; width:80%; border: 2px #9A7BFF dashed;\" border=\"1\"><tbody>");
		for (String s : genericaData.getQueryParameter()) {
			HTMLStyle.append("<tr><td style=\"width: 80%;\">" + s + "</td></tr>");
		}
		HTMLStyle.append("</tbody></table>");
		
		// body table
		//HTMLStyle.append("<p>&nbsp;</p>");
		HTMLStyle.append("<p>BODY</p><textarea rows=\"7\" cols=\"70\">");
		if (genericaData.getRequestBODY() != null)
			HTMLStyle.append(genericaData.getRequestBODY());
		
		HTMLStyle.append("</textarea></html>");
		
		return HTMLStyle.toString();
	}

	@Override
	public String getJSON() {
		try {
			return new ObjectMapper().writeValueAsString(getGenericData());
			//return new ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(getGenericData());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public String getXML(Class c) throws JsonProcessingException, JAXBException {
		ObjectMapper objectMapper = new XmlMapper();
		return objectMapper.writeValueAsString(getGenericData());
		
		//Create JAXB Context
        /*JAXBContext jaxbContext = JAXBContext.newInstance(c);
         
        //Create Marshaller
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

        //Required formatting??
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        //Print XML String to Console
        StringWriter sw = new StringWriter();
         
        //Write XML to StringWriter
        jaxbMarshaller.marshal(getGenericData(), sw);
         
        //Verify XML Content
        String xmlContent = sw.toString();
        //System.out.println( xmlContent );
        return xmlContent;*/
	}
	
	
}
